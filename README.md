# README #

### What is this repository for? ###

Multiplayer action gaming often means a server which is able to update clients with world state snapshots several times per second.
I want to learn how these snapshots can be serialized and sent to client over UDP.
The goals of POC are:

1. Learn basic steps of multiplayer game server implementation techniques. (error handling, interpolation, etc)  

2. Learn Netty framework

3. Try to understand how Quasar framework may help to reduce the server load

4. Receive more experience with Kotlin