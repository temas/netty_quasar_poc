package com.temas.netty.poc

import java.io.Serializable

/**
 * @author Artem Zhdanov <a.a.zhdanov@gmail.com>
 * @since 1/7/2016
 */


data class WorldState(val time: Long, val gridModel: Array<Array<Int>>) : Serializable {
    override fun toString(): String = "Time: $time, gridModel ${gridModel.map { it.map { it.toString() } }}"
}