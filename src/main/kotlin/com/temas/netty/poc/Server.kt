package com.temas.netty.poc

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.channel.ChannelInitializer
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.serialization.ClassResolvers
import io.netty.handler.codec.serialization.ObjectDecoder
import io.netty.handler.codec.serialization.ObjectEncoder
import io.netty.handler.logging.LogLevel
import io.netty.handler.logging.LoggingHandler

/**
 * @author Artem Zhdanov <a.a.zhdanov@gmail.com>
 * @since 1/7/2016
 */

public class Server {

    companion object {
        @JvmStatic public fun main (args: Array<String>) {
            Server().start(args);
        }
    }

    val PORT = Integer.parseInt(System.getProperty("port", "8080"));

    fun start(args: Array<String>) {
        val bossGroup = NioEventLoopGroup(1)
        val workerGroup = NioEventLoopGroup()
        try {
            val b = ServerBootstrap()
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel::class.java)
                            .handler(LoggingHandler(LogLevel.INFO))
                            .childHandler(object : ChannelInitializer<SocketChannel>() {
                                override fun initChannel(ch : SocketChannel)  {
                                    val p = ch.pipeline();

                                    p.addLast(
                                            ObjectEncoder(),
                                            ObjectDecoder(ClassResolvers.cacheDisabled(null)),
                                            ObjectEchoServerHandler())
                                }
                            })

            // Bind and start to accept incoming connections.
            b.bind(PORT).sync().channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

class ObjectEchoServerHandler : ChannelInboundHandlerAdapter() {
    override fun channelRead(ctx: ChannelHandlerContext?, msg: Any?) {
        ctx?.write(msg)
        println("com.temas.netty.poc.Server read $msg")
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext?) {
        ctx?.flush()
        println("Server flushed")
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        cause?.printStackTrace()
        ctx?.close()
    }

}
