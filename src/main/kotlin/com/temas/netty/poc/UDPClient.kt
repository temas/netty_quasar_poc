package com.temas.netty.poc

import io.netty.bootstrap.Bootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.DatagramPacket
import io.netty.channel.socket.nio.NioDatagramChannel
import io.netty.handler.codec.MessageToMessageDecoder
import io.netty.handler.codec.protobuf.ProtobufDecoder
import io.netty.handler.codec.protobuf.ProtobufEncoder
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender
import io.netty.util.ReferenceCountUtil
import org.slf4j.LoggerFactory

/**
 * @author Artem Zhdanov <a.a.zhdanov@gmail.com>
 * @since 1/9/2016
 */

class UDPClient {
    companion object {
        @JvmStatic public fun main (args: Array<String>) {
            UDPClient().start(args);
        }
    }

    val HOST = System.getProperty("host", "127.0.0.1");
    val PORT = Integer.parseInt(System.getProperty("port", "8080"));
    var connectCounter = 5;

    fun start(args: Array<String>) {
        val group = NioEventLoopGroup(1);
        try {
            val b = Bootstrap();
            b.group(group)
                    .channel(NioDatagramChannel::class.java)
                    .handler(object : ChannelInitializer<NioDatagramChannel>() {
                        override fun initChannel(ch : NioDatagramChannel){
                            val p = ch.pipeline();
                            p.addLast(
                                    object : MessageToMessageDecoder<DatagramPacket>() {
                                        override fun decode(ctx: ChannelHandlerContext, msg: DatagramPacket, out: MutableList<Any>) {
                                            out.add(msg.content().retain())
                                        }
                                    },
                                    ProtobufVarint32LengthFieldPrepender(),
                                    ProtobufEncoder(),
                                    ProtobufVarint32FrameDecoder(),
                                    ProtobufDecoder(WorldStateProtoOuterClass.WorldStateProto.getDefaultInstance()),
                                    UDPObjectEchoClientHandler());
                        }
                    });

            // Start the connection attempt.
            val channelFuture = b.connect(HOST, PORT)
            channelFuture.addListener(object : ChannelFutureListener {
                override fun operationComplete(future: ChannelFuture) {
                    if (!future.isSuccess) {
                        if (--connectCounter < 0) {
                            println("Error connecting to $HOST:$PORT")
                        } else {
                            Thread.sleep(1000)
                            println("Reconnecting to $HOST:$PORT ...")
                            b.connect(HOST, PORT).addListener(this)
                        }
                    }
                }
            })
            channelFuture.sync().channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully();
        }
    }
}


class UDPObjectEchoClientHandler : ChannelInboundHandlerAdapter() {
    val logger = LoggerFactory.getLogger(UDPObjectEchoClientHandler::class.java);

    val grid  = Array(10, { i -> Array(10, { j -> j * i.toInt() }) })
    val state = WorldStateProtoOuterClass.WorldStateProto.newBuilder()
            .setTimestamp(System.currentTimeMillis())
            .addAllGrid(grid.flatMap { it.asList() }).build()

    override fun channelActive(ctx: ChannelHandlerContext) {
        ctx.writeAndFlush(state)
        println("com.temas.netty.poc.Client write to $ctx")
    }

    override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
        try {
            println("com.temas.netty.poc.Client read msg $msg")
        } finally {
            ReferenceCountUtil.release(msg)
        }
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext) {
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        logger.error("Unexpected error in UDPObjectEchoServerHandler", cause)
    }
}


