package com.temas.netty.poc

import io.netty.bootstrap.Bootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import io.netty.handler.codec.serialization.ClassResolvers
import io.netty.handler.codec.serialization.ObjectDecoder
import io.netty.handler.codec.serialization.ObjectEncoder
import java.net.ConnectException

/**
 * @author Artem Zhdanov <a.a.zhdanov@gmail.com>
 * @since 1/7/2016
 */

class Client {

    companion object {
        @JvmStatic public fun main (args: Array<String>) {
            Client().start(args);
        }
    }

    val HOST = System.getProperty("host", "127.0.0.1");
    val PORT = Integer.parseInt(System.getProperty("port", "8080"));
    var connectCounter = 5;

    fun start(args: Array<String>) {
        val group = NioEventLoopGroup();
        try {
            val b = Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel::class.java)
                    .handler(object : ChannelInitializer<SocketChannel>() {
                        override fun initChannel(ch: SocketChannel) {
                            val p = ch.pipeline();
                            p.addLast(
                                    ObjectEncoder(),
                                    ObjectDecoder(ClassResolvers.cacheDisabled(null)),
                                    ObjectEchoClientHandler());
                        }
                    });

            // Start the connection attempt.
            val channelFuture = b.connect(HOST, PORT)
            channelFuture.addListener(object : ChannelFutureListener {
                override fun operationComplete(future: ChannelFuture) {
                    if (!future.isSuccess) {
                        if (--connectCounter < 0) {
                            println("Error connecting to $HOST:$PORT")
                        } else {
                            println("Reconnecting to $HOST:$PORT ...")
                            b.connect(HOST, PORT).addListener(this)
                        }
                    }
                }
            })
            if (channelFuture.isSuccess) {
                channelFuture.sync().channel().closeFuture().sync();
            }
        } catch (e : ConnectException) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }
}

class ObjectEchoClientHandler : ChannelInboundHandlerAdapter() {
    val state  = WorldState(System.currentTimeMillis(), Array(10, { i -> Array(10, { j -> j * i.toInt() }) }))

    override fun channelActive(ctx: ChannelHandlerContext?) {
        ctx?.writeAndFlush(state)
        println("com.temas.netty.poc.Client write to $ctx")
    }

    override fun channelRead(ctx: ChannelHandlerContext?, msg: Any?) {
        //ctx?.write(msg)
        println("com.temas.netty.poc.Client read msg $msg")
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext?) {
        //ctx?.flush()
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        cause?.printStackTrace();
        ctx?.close();
    }
}
